## Database will be create automatically
## Please check database userid and password
## Sample Valid JSON Request Bodys and Api details

#new user signup
#Method: POST
##### url: localhost:8080/api/auth/signup
```json
{
	"firstName": "Sarwar",
	"lastName": "Rasel",
	"username": "sarwar",
	"password": "password",
	"email": "sarwar@gmail.com"
}
```

#signin/login
#Method: POST
##### url: localhost:8080/api/auth/signin
```json
{
	"usernameOrEmail": "sarwar",
	"password": "password"
}
```
#create new user
#Method: POST
##### url: localhost:8080/api/users
```json
{
	"firstName": "Ervin",
	"lastName": "Howell",
	"username": "ervin",
	"password": "password",
	"email": "ervin.howell@gmail.com"
}
```
#user edit/update
#Method: PUT
##### url: localhost:8080/api/users/{username}
```json
{
	"firstName": "Ervin",
	"lastName": "Howell",
	"username": "ervin",
	"password": "updatedpassword",
	"email": "ervin.howell@gmail.com"
	
}
```

#find project by user
#Method: GET
##### url: localhost:8080/api/users/{username}/projects

#find task by user
#Method: GET
##### url: localhost:8080/api/users/{username}/tasks

#create new project
#Method: POST
##### url: localhost:8080/api/project/
```json
{
	"name": "Project1"
}
```

#edit/update project
#Method: PUT
##### url: localhost:8080/api/project/{id}
```json
{
	"name": "Project1"
}
```

#get all project
#Method: GET
##### url: localhost:8080/api/project

#get specific project by id
#Method: GET
##### url: localhost:8080/api/project/{id}



#create new task
#Method: POST
##### url: localhost:8080/api/task
```json
{
	"title": "Task1",
	"description": "Task1 Description",
	"projectId": 1,
    "userId":1,
	"status": "Open",
	"duedate": "2020-02-02"
}
```

#edit/update a task
#Method: PUT
##### url: localhost:8080/api/task/{id}
```json
{
	"title": "Task2",
	"description": "Task2 Description",
	"projectId": 1,
    "userId":1,
	"status": "in progress",
	"duedate": "2020-02-02"
}
```

#get all tasks
#Method: GET
##### url: localhost:8080/api/task

#get specific task by id
#Method: GET
##### url: localhost:8080/api/task/{id}

#get tasks by project id
#Method: GET
##### url: localhost:8080/api/task/project/{id}


#get task by status
#Method: GET
##### url: localhost:8080/api/task/findstatus/{status}

#get task by due date
#Method: GET
##### url: localhost:8080/api/task/findduedate/{duedate}