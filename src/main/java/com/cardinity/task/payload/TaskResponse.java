package com.cardinity.task.payload;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
public class TaskResponse {
	private String title;
	private String description;
//	private String project;
	private String user;
	private String status;
	private Date duedate;

}
