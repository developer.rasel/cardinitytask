package com.cardinity.task.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
public class TaskRequest {

	@NotBlank
	private String title;

	@NotBlank
	private String description;

	@NotNull
	private Long projectId;

	@NotNull
	private Long userId;

	@NotBlank
	private String status;


	@JsonFormat(pattern="yyyy-MM-dd")
	private Date duedate;
}
