package com.cardinity.task.repository;



import com.cardinity.task.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
	Page<Task> findByCreatedBy(Long userId, Pageable pageable);

	Page<Task> findByProjectId(Long projectId, Pageable pageable);

	Page<Task> findByStatus(String status, Pageable pageable);

	Page<Task> findByDuedate(Date duedate, Pageable pageable);

	Long countByCreatedBy(Long userId);
}
