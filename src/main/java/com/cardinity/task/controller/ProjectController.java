package com.cardinity.task.controller;


import com.cardinity.task.exception.UnauthorizedException;
import com.cardinity.task.model.Project;
import com.cardinity.task.payload.ApiResponse;
import com.cardinity.task.payload.PagedResponse;
import com.cardinity.task.security.CurrentUser;
import com.cardinity.task.security.UserPrincipal;
import com.cardinity.task.service.ProjectService;
import com.cardinity.task.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/project")
public class ProjectController {
	@Autowired
	private ProjectService projectService;

	@GetMapping
	public PagedResponse<Project> getAllProjects(
			@RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
			@RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
		return projectService.getAllProject(page, size);
	}

	@PostMapping
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Project> addProject(@Valid @RequestBody Project project,
											   @CurrentUser UserPrincipal currentUser) {

		return projectService.addProject(project, currentUser);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Project> getProject(@PathVariable(name = "id") Long id) {
		return projectService.getProject(id);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<Project> updateProject(@PathVariable(name = "id") Long id,
												  @Valid @RequestBody Project project, @CurrentUser UserPrincipal currentUser) throws UnauthorizedException {
		return projectService.updateProject(id, project, currentUser);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<ApiResponse> deleteProject(@PathVariable(name = "id") Long id,
													 @CurrentUser UserPrincipal currentUser) throws UnauthorizedException {
		return projectService.deleteProject(id, currentUser);
	}

}
