package com.cardinity.task.controller;

import com.cardinity.task.model.Task;
import com.cardinity.task.payload.ApiResponse;
import com.cardinity.task.payload.PagedResponse;
import com.cardinity.task.payload.TaskRequest;
import com.cardinity.task.payload.TaskResponse;
import com.cardinity.task.security.CurrentUser;
import com.cardinity.task.security.UserPrincipal;
import com.cardinity.task.service.TaskService;
import com.cardinity.task.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/api/task")
public class TaskController {
	@Autowired
	private TaskService taskService;

	@GetMapping
	public ResponseEntity<PagedResponse<Task>> getAllTasks(
			@RequestParam(value = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
			@RequestParam(value = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
		PagedResponse<Task> response = taskService.getAllPosts(page, size);

		return new ResponseEntity< >(response, HttpStatus.OK);
	}

	@GetMapping("/project/{id}")
	public ResponseEntity<PagedResponse<Task>> getTasksByProject(
			@RequestParam(value = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
			@RequestParam(value = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size,
			@PathVariable(name = "id") Long id) {
		System.out.println("projectid"+id);
		PagedResponse<Task> response = taskService.getPostsByProject(id, page, size);

		return new ResponseEntity< >(response, HttpStatus.OK);
	}

	@GetMapping("/findstatus/{status}")
	public ResponseEntity<PagedResponse<Task>> getTaskByStatus(
			@RequestParam(value = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
			@RequestParam(value = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size,
			@PathVariable(name = "status") String status) {
		PagedResponse<Task> response = taskService.getPostsByStatus(status, page, size);

		return new ResponseEntity< >(response, HttpStatus.OK);
	}

	@GetMapping("/findduedate/{duedate}")
	public ResponseEntity<PagedResponse<Task>> getTaskByDueDate(
			@RequestParam(value = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
			@RequestParam(value = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size,
			@PathVariable(name = "status") Date duedate) {
		PagedResponse<Task> response = taskService.getPostsByDueDate(duedate, page, size);

		return new ResponseEntity< >(response, HttpStatus.OK);
	}



	@PostMapping
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<TaskResponse> addTask(@Valid @RequestBody TaskRequest taskRequest,
												@CurrentUser UserPrincipal currentUser) {
		TaskResponse taskResponse = taskService.addPost(taskRequest, currentUser);

		return new ResponseEntity< >(taskResponse, HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Task> getTask(@PathVariable(name = "id") Long id) {
		Task task = taskService.getPost(id);

		return new ResponseEntity< >(task, HttpStatus.OK);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<Task> updateTask(@PathVariable(name = "id") Long id,
										   @Valid @RequestBody TaskRequest newTaskRequest, @CurrentUser UserPrincipal currentUser) {

			Task posts = taskService.updatePost(id, newTaskRequest, currentUser);

			return new ResponseEntity< >(posts, HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<ApiResponse> deleteTask(@PathVariable(name = "id") Long id, @CurrentUser UserPrincipal currentUser) {
		ApiResponse apiResponse = taskService.deletePost(id, currentUser);

		return new ResponseEntity< >(apiResponse, HttpStatus.OK);
	}
}
