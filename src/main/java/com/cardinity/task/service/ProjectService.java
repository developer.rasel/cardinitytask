package com.cardinity.task.service;

import com.cardinity.task.exception.UnauthorizedException;
import com.cardinity.task.model.Project;
import com.cardinity.task.payload.ApiResponse;
import com.cardinity.task.payload.PagedResponse;
import com.cardinity.task.security.UserPrincipal;
import org.springframework.http.ResponseEntity;

public interface ProjectService {

	PagedResponse<Project> getAllProject(int page, int size);

	ResponseEntity<Project> getProject(Long id);

	ResponseEntity<Project> addProject(Project project, UserPrincipal currentUser);

	ResponseEntity<Project> updateProject(Long id, Project newProject, UserPrincipal currentUser)
			throws UnauthorizedException;

	ResponseEntity<ApiResponse> deleteProject(Long id, UserPrincipal currentUser) throws UnauthorizedException;

	PagedResponse<Project> getProjectsByCreatedBy(String username, Integer page, Integer size);
}
