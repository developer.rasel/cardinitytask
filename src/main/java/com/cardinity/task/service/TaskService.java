package com.cardinity.task.service;



import com.cardinity.task.model.Task;
import com.cardinity.task.payload.ApiResponse;
import com.cardinity.task.payload.PagedResponse;
import com.cardinity.task.payload.TaskRequest;
import com.cardinity.task.payload.TaskResponse;
import com.cardinity.task.security.UserPrincipal;

import java.util.Date;

public interface TaskService {

	PagedResponse<Task> getAllPosts(int page, int size);

	PagedResponse<Task> getPostsByCreatedBy(String username, int page, int size);

	PagedResponse<Task> getPostsByProject(Long id, int page, int size);
	PagedResponse<Task> getPostsByStatus(String status, int page, int size);
	PagedResponse<Task> getPostsByDueDate(Date duedate, int page, int size);


	Task updatePost(Long id, TaskRequest newTaskRequest, UserPrincipal currentUser);

	ApiResponse deletePost(Long id, UserPrincipal currentUser);

	TaskResponse addPost(TaskRequest taskRequest, UserPrincipal currentUser);

	Task getPost(Long id);

}
