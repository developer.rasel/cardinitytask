package com.cardinity.task.service;


import com.cardinity.task.model.user.User;
import com.cardinity.task.payload.ApiResponse;
import com.cardinity.task.payload.UserIdentityAvailability;
import com.cardinity.task.payload.UserProfile;
import com.cardinity.task.payload.UserSummary;
import com.cardinity.task.security.UserPrincipal;

public interface UserService {

	UserSummary getCurrentUser(UserPrincipal currentUser);

	UserIdentityAvailability checkUsernameAvailability(String username);

	UserIdentityAvailability checkEmailAvailability(String email);

	UserProfile getUserProfile(String username);

	User addUser(User user);

	User updateUser(User newUser, String username, UserPrincipal currentUser);

	ApiResponse deleteUser(String username, UserPrincipal currentUser);

	ApiResponse giveAdmin(String username);

	ApiResponse removeAdmin(String username);


}