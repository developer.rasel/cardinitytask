package com.cardinity.task.service.impl;

import com.cardinity.task.exception.ResourceNotFoundException;
import com.cardinity.task.exception.UnauthorizedException;
import com.cardinity.task.model.Project;
import com.cardinity.task.model.role.RoleName;
import com.cardinity.task.model.user.User;
import com.cardinity.task.payload.ApiResponse;
import com.cardinity.task.payload.PagedResponse;
import com.cardinity.task.repository.ProjectRepository;
import com.cardinity.task.repository.UserRepository;
import com.cardinity.task.security.UserPrincipal;
import com.cardinity.task.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static com.cardinity.task.utils.AppConstants.CREATED_AT;
import static com.cardinity.task.utils.AppUtils.validatePageNumberAndSize;


@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public PagedResponse<Project> getAllProject(int page, int size) {
		validatePageNumberAndSize(page, size);

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");

		Page<Project> categories = projectRepository.findAll(pageable);

		List<Project> content = categories.getNumberOfElements() == 0 ? Collections.emptyList() : categories.getContent();

		return new PagedResponse<>(content, categories.getNumber(), categories.getSize(), categories.getTotalElements(),
				categories.getTotalPages(), categories.isLast());
	}

	@Override
	public ResponseEntity<Project> getProject(Long id) {
		Project project = projectRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Project", "id", id));
		return new ResponseEntity<>(project, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Project> addProject(Project project, UserPrincipal currentUser) {
		Project newProject = projectRepository.save(project);
		return new ResponseEntity<>(newProject, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<Project> updateProject(Long id, Project newProject, UserPrincipal currentUser) {
		Project project = projectRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Project", "id", id));
		if (project.getCreatedBy().equals(currentUser.getId()) || currentUser.getAuthorities()
				.contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
			project.setName(newProject.getName());
			Project updatedProject = projectRepository.save(project);
			return new ResponseEntity<>(updatedProject, HttpStatus.OK);
		}

		throw new UnauthorizedException("You don't have permission to edit this Project");
	}

	@Override
	public ResponseEntity<ApiResponse> deleteProject(Long id, UserPrincipal currentUser) {
		Project project = projectRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Project", "id", id));
		if (project.getCreatedBy().equals(currentUser.getId()) || currentUser.getAuthorities()
				.contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
			projectRepository.deleteById(id);
			return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "You successfully deleted Project"), HttpStatus.OK);
		}
		throw new UnauthorizedException("You don't have permission to delete this Project");
	}

	@Override
	public PagedResponse<Project> getProjectsByCreatedBy(String username, Integer page, Integer size) {
		validatePageNumberAndSize(page, size);
		User user = userRepository.getUserByName(username);
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, CREATED_AT);
		Page<Project> projects = projectRepository.findByCreatedBy(user.getId(), pageable);

		List<Project> content = projects.getNumberOfElements() == 0 ? Collections.emptyList() : projects.getContent();

		return new PagedResponse<>(content, projects.getNumber(), projects.getSize(), projects.getTotalElements(),
				projects.getTotalPages(), projects.isLast());
	}

}






















