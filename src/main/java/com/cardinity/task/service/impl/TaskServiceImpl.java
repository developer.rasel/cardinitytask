package com.cardinity.task.service.impl;



import com.cardinity.task.exception.BadRequestException;
import com.cardinity.task.exception.ResourceNotFoundException;
import com.cardinity.task.exception.UnauthorizedException;
import com.cardinity.task.model.Project;
import com.cardinity.task.model.Task;
import com.cardinity.task.model.role.RoleName;
import com.cardinity.task.model.user.User;
import com.cardinity.task.payload.ApiResponse;
import com.cardinity.task.payload.PagedResponse;
import com.cardinity.task.payload.TaskRequest;
import com.cardinity.task.payload.TaskResponse;
import com.cardinity.task.repository.ProjectRepository;
import com.cardinity.task.repository.TaskRepository;
import com.cardinity.task.repository.UserRepository;
import com.cardinity.task.security.UserPrincipal;
import com.cardinity.task.service.TaskService;
import com.cardinity.task.utils.AppConstants;
import com.cardinity.task.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.cardinity.task.utils.AppConstants.*;


@Service
public class TaskServiceImpl implements TaskService {
	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProjectRepository projectRepository;


	@Override
	public PagedResponse<Task> getAllPosts(int page, int size) {
		validatePageNumberAndSize(page, size);

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, CREATED_AT);

		Page<Task> posts = taskRepository.findAll(pageable);

		List<Task> content = posts.getNumberOfElements() == 0 ? Collections.emptyList() : posts.getContent();

		return new PagedResponse<>(content, posts.getNumber(), posts.getSize(), posts.getTotalElements(),
				posts.getTotalPages(), posts.isLast());
	}

	@Override
	public PagedResponse<Task> getPostsByCreatedBy(String username, int page, int size) {
		validatePageNumberAndSize(page, size);
		User user = userRepository.getUserByName(username);
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, CREATED_AT);
		Page<Task> posts = taskRepository.findByCreatedBy(user.getId(), pageable);

		List<Task> content = posts.getNumberOfElements() == 0 ? Collections.emptyList() : posts.getContent();

		return new PagedResponse<>(content, posts.getNumber(), posts.getSize(), posts.getTotalElements(),
				posts.getTotalPages(), posts.isLast());
	}

	@Override
	public PagedResponse<Task> getPostsByProject(Long id, int page, int size) {
		AppUtils.validatePageNumberAndSize(page, size);
		Project project = projectRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(CATEGORY, ID, id));

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, CREATED_AT);
		Page<Task> posts = taskRepository.findByProjectId(project.getId(), pageable);

		List<Task> content = posts.getNumberOfElements() == 0 ? Collections.emptyList() : posts.getContent();

		return new PagedResponse<>(content, posts.getNumber(), posts.getSize(), posts.getTotalElements(),
				posts.getTotalPages(), posts.isLast());
	}

	@Override
	public PagedResponse<Task> getPostsByStatus(String status, int page, int size) {
		AppUtils.validatePageNumberAndSize(page, size);
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, CREATED_AT);
		Page<Task> posts = taskRepository.findByStatus(status, pageable);

		List<Task> content = posts.getNumberOfElements() == 0 ? Collections.emptyList() : posts.getContent();

		return new PagedResponse<>(content, posts.getNumber(), posts.getSize(), posts.getTotalElements(),
				posts.getTotalPages(), posts.isLast());
	}

	@Override
	public PagedResponse<Task> getPostsByDueDate(Date duedate, int page, int size) {
		AppUtils.validatePageNumberAndSize(page, size);
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, CREATED_AT);
		Page<Task> posts = taskRepository.findByDuedate(duedate, pageable);

		List<Task> content = posts.getNumberOfElements() == 0 ? Collections.emptyList() : posts.getContent();

		return new PagedResponse<>(content, posts.getNumber(), posts.getSize(), posts.getTotalElements(),
				posts.getTotalPages(), posts.isLast());
	}


	@Override
	public Task updatePost(Long id, TaskRequest newTaskRequest, UserPrincipal currentUser) {
		Task task = taskRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(POST, ID, id));
		Project project = projectRepository.findById(newTaskRequest.getProjectId()).get();
//				.orElseThrow(() -> new ResourceNotFoundException(CATEGORY, ID, newTaskRequest.getProjectId()));
		if (task.getUser().getId().equals(currentUser.getId())
				|| currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
			if (task.getStatus().equalsIgnoreCase("closed")){
				ApiResponse apiResponse = new ApiResponse(Boolean.FALSE, "This is a closed project");

				throw new UnauthorizedException(apiResponse);
			}else {
				task.setTitle(newTaskRequest.getTitle());
				task.setDescription(newTaskRequest.getDescription());
				task.setProject(project);
				task.setStatus(newTaskRequest.getStatus());
				task.setDuedate(newTaskRequest.getDuedate());
				return taskRepository.save(task);
			}

		}
		ApiResponse apiResponse = new ApiResponse(Boolean.FALSE, "You don't have permission to edit this post");

		throw new UnauthorizedException(apiResponse);
	}

	@Override
	public ApiResponse deletePost(Long id, UserPrincipal currentUser) {
		Task task = taskRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(POST, ID, id));
		if (task.getUser().getId().equals(currentUser.getId())
				|| currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
			taskRepository.deleteById(id);
			return new ApiResponse(Boolean.TRUE, "You successfully deleted post");
		}

		ApiResponse apiResponse = new ApiResponse(Boolean.FALSE, "You don't have permission to delete this post");

		throw new UnauthorizedException(apiResponse);
	}

	@Override
	public TaskResponse addPost(TaskRequest taskRequest, UserPrincipal currentUser) {
		User user = userRepository.findById(taskRequest.getUserId())
				.orElseThrow(() -> new ResourceNotFoundException(USER, ID, 1L));
		Project project = projectRepository.findById(taskRequest.getProjectId())
				.orElseThrow(() -> new ResourceNotFoundException(CATEGORY, ID, taskRequest.getProjectId()));


		Task task = new Task();
		task.setDescription(taskRequest.getDescription());
		task.setTitle(taskRequest.getTitle());
		task.setProject(project);
		task.setStatus(taskRequest.getStatus());
		task.setDuedate(taskRequest.getDuedate());
		task.setUser(user);

		Task newTask = taskRepository.save(task);

		TaskResponse taskResponse = new TaskResponse();

		taskResponse.setTitle(newTask.getTitle());
		taskResponse.setDescription(newTask.getDescription());
		taskResponse.setStatus(newTask.getStatus());
		taskResponse.setDuedate(newTask.getDuedate());
		taskResponse.setUser(newTask.getUser().getLastName());


		return taskResponse;
	}

	@Override
	public Task getPost(Long id) {
		return taskRepository.findById(id).get();
//				.orElseThrow(() -> new ResourceNotFoundException(POST, ID, id));
	}

	private void validatePageNumberAndSize(int page, int size) {
		if (page < 0) {
			throw new BadRequestException("Page number cannot be less than zero.");
		}

		if (size < 0) {
			throw new BadRequestException("Size number cannot be less than zero.");
		}

		if (size > AppConstants.MAX_PAGE_SIZE) {
			throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
		}
	}
}
